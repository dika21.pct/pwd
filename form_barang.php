<h2>Tambah Data Barang</h2>
<form action="add_barang.php" method="post">
<div class="mb-3">
  <label class="form-label">Kode Barang</label>
  <input type="text" class="form-control" name="kode">
</div>
<div class="mb-3">
  <label class="form-label">Nama Barang</label>
  <input type="text" class="form-control" name="nama_brg">
</div>
<div class="mb-3">
  <label class="form-label">Jenis Barang</label>
  <select class="form-control" name="jenis_brg">
  <?php
    include "koneksi.php";
    $sql_tampil = "SELECT * FROM jenis_barang";
    $query = mysqli_query($koneksi,$sql_tampil);
    while($data_jenis=mysqli_fetch_array($query)){
    ?>
    <option value="<?php echo $data_jenis['kd_jenis']; ?>">
    <?php echo $data_jenis['nm_jenis']; ?></option>
    <?php } ?>
  </select>
</div>
<div class="mb-3">
  <label class="form-label">Harga Barang</label>
  <input type="text" class="form-control" name="harga_brg">
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>