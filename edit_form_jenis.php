<?php
    include "koneksi.php";
    $kode = $_GET['kode'];
    $sql_tampil = "SELECT * FROM jenis_barang WHERE kd_jenis='$kode'";
    $query = mysqli_query($koneksi,$sql_tampil);
    $data_jenis=mysqli_fetch_array($query);
?>
<h2>Form Ubah Data Jenis</h2>
<form action="update_jenis.php" method="post">
<div class="mb-3">
  <label class="form-label">Kode Jenis Barang</label>
  <input type="text" class="form-control" name="kode"
  value="<?php echo $data_jenis['kd_jenis']; ?>" readonly>
</div>
<div class="mb-3">
  <label class="form-label">Nama Jenis Barang</label>
  <input type="text" class="form-control" name="nama"
  value="<?php echo $data_jenis['nm_jenis']; ?>">
</div>
<button type="submit" class="btn btn-primary">Update</button>
</form>