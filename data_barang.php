<h2>Data Barang</h2>
<table class="table">
    <tr>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Jenis Barang</th>
        <th>Stok</th>
        <th>Harga</th>
    </tr>
    <?php
    include "koneksi.php";
    $sql_tampil = "SELECT jenis_barang.nm_jenis,barang.kd_barang,
    nm_barang,harga,stok from jenis_barang INNER JOIN barang 
    ON jenis_barang.kd_jenis=barang.kd_jenis";
    $query = mysqli_query($koneksi,$sql_tampil);
    while($data=mysqli_fetch_array($query)){
    ?>
    <tr>
        <td><?php echo $data['kd_barang'];?></td>
        <td><?php echo $data['nm_barang'];?></td>
        <td><?php echo $data['nm_jenis'];?></td>
        <td><?php echo $data['stok'];?></td>
        <td><?php echo number_format($data['harga']);?></td>
    </tr>
    <?php } ?>
</table>