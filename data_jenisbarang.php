<h1>Data Jenis Barang</h1>
<table class="table">
    <tr>
        <th>ID Jenis Barang</th>
        <th>Nama Jenis Barang</th>
        <th>Action</th>
    </tr>
    <?php
    include "koneksi.php";
    $sql_tampil = "SELECT * FROM jenis_barang";
    $query = mysqli_query($koneksi,$sql_tampil);
    while($data_jenis=mysqli_fetch_array($query)){
    ?>
    <tr>
        <td><?php echo $data_jenis['kd_jenis']; ?></td>
        <td><?php echo $data_jenis['nm_jenis']; ?></td>
        <td><a href="index.php?nm_link=fe_jenis&kode=<?php echo $data_jenis['kd_jenis']; ?>" class="btn btn-outline-primary">Edit</a> <a href="delete_jenisbrg.php?kode=<?php echo $data_jenis['kd_jenis']; ?>" class="btn btn-outline-primary">Delete</a></td>
    </tr>
    <?php } ?>
</table>