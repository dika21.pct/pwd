-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2022 at 12:05 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `palcomtech`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kd_barang` varchar(3) NOT NULL,
  `nm_barang` varchar(20) DEFAULT NULL,
  `kd_jenis` varchar(3) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `stok` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kd_barang`, `nm_barang`, `kd_jenis`, `harga`, `stok`) VALUES
('A10', 'Cireng', 'A09', 2500, NULL),
('B01', 'Pensil', 'A01', 5000, 10),
('B02', 'Pena', 'A01', 6000, 10),
('B03', 'Mesin Cuci', 'A02', 600000, 10),
('B04', 'Kulkas', 'A02', 700000, 10),
('B05', 'Sejarah', 'A03', 70000, 10),
('B12', 'penggaris', 'A01', 8500, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_barang`
--

CREATE TABLE `data_barang` (
  `kd_barang` varchar(4) NOT NULL,
  `nm_barang` varchar(20) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `data_barang`
--

INSERT INTO `data_barang` (`kd_barang`, `nm_barang`, `harga_beli`, `harga_jual`, `stock`) VALUES
('B001', 'Pena', 1500, 2000, 150),
('B002', 'Pensil', 1000, 1500, 200),
('B003', 'Buku', 2000, 2500, 170),
('B004', 'Buku Gambar', 2000, 2500, 170);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kd_jenis` varchar(3) NOT NULL,
  `nm_jenis` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kd_jenis`, `nm_jenis`) VALUES
('A01', 'ATK'),
('A02', 'Elektronik'),
('A08', 'Sembako2');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `NPM` int(8) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `jurusan` varchar(20) DEFAULT NULL,
  `alamat` varchar(20) DEFAULT NULL,
  `hp` varchar(13) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`NPM`, `nama`, `jurusan`, `alamat`, `hp`, `email`) VALUES
(1234576, 'Dina', 'D3 AK', 'Sekayu', '081245458989', 'dina_ans@palcomtech.com'),
(1234577, 'Dina', 'D3 AK', 'Sekayu', '081245458989', 'dina@palcomtech.com'),
(1235689, 'Bayu', 'D3 SI', 'Palembang', NULL, 'bayu@gmail.com'),
(12345678, 'Dini', 'D3 SI', 'Palembang', '08127168977', 'dini@palcomtech.ac.id'),
(12345679, 'Mayang', 'D3 SI', 'Palembang', '081278784545', 'mayang@palcomtech.com');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_dini`
--

CREATE TABLE `nilai_dini` (
  `NIS` int(3) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `nilai_bi` int(3) DEFAULT NULL,
  `nilai_be` int(3) DEFAULT NULL,
  `nilai_ps` int(3) DEFAULT NULL,
  `nilai_pa` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nilai_dini`
--

INSERT INTO `nilai_dini` (`NIS`, `nama`, `nilai_bi`, `nilai_be`, `nilai_ps`, `nilai_pa`) VALUES
(1, 'Dea Anika Afrilia', 89, 0, 0, 0),
(2, 'M Alfian', 89, 56, 56, 20),
(3, 'dd', 78, 89, 58, 80),
(4, 'M Alfian', 78, 78, 78, 78),
(5, 'Weni Kurniati', 45, 50, 89, 78),
(6, 'linda', 78, 88, 88, 89),
(7, 'Iwan Diantoro', 89, 78, 78, 56),
(8, 'Nadia', 89, 23, 56, 89),
(9, 'Weni Kurniati', 89, 59, 78, 88);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(3) NOT NULL,
  `nama_pegawai` varchar(30) DEFAULT NULL,
  `jabatan_pegawai` varchar(20) DEFAULT NULL,
  `gapok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `jabatan_pegawai`, `gapok`) VALUES
(1, 'Irham Frasdiansyah', 'Manager', 0),
(2, 'Damar Fajery', 'Direktur', 0),
(3, 'Weni Kurniati', 'Supervisor', 0),
(4, 'zzz', 'Direktur', 0),
(5, 'Weni Kurniati', 'Staff', 0),
(6, 'Iwan Diantoro', 'Manager', 0),
(7, 'M Alfian', 'Direktur', 0),
(8, 'Iwan Diantoro', 'Supervisor', 4500000),
(9, 'Dea Anika Afrilia', 'Staff', 3000000),
(10, 'M Alfian', 'Manager', 6000000);

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id_jurusan` int(2) NOT NULL,
  `nm_jurusan` varchar(20) DEFAULT NULL,
  `spp` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id_jurusan`, `nm_jurusan`, `spp`) VALUES
(123, 'D3 SI', 7500000),
(124, 'D3 DKV', 8500000),
(125, 'D3 AK', 1000000),
(126, 'S1 SI', 1100000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indexes for table `data_barang`
--
ALTER TABLE `data_barang`
  ADD PRIMARY KEY (`kd_barang`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`kd_jenis`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`NPM`);

--
-- Indexes for table `nilai_dini`
--
ALTER TABLE `nilai_dini`
  ADD PRIMARY KEY (`NIS`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nilai_dini`
--
ALTER TABLE `nilai_dini`
  MODIFY `NIS` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
